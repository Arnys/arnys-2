﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DGVPrinterHelper;

namespace VideoKameruRegistrs
{
    public partial class VideoKameras : Form
    {
        public string strMyOriginalText { get; private set; }

        public VideoKameras()
        {
            InitializeComponent();
        }

        private void videoKamerasBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.videoKamerasBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.vidKamDataSet);

        }

        private void VideoKameras_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'vidKamDataSet.VideoKameras' table. You can move, or remove it, as needed.
            this.videoKamerasTableAdapter.Fill(this.vidKamDataSet.VideoKameras);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            bilde f3 = new bilde();
            f3.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            printPreviewDialog1.ShowDialog();
            i = 0;
            
        }

        private void VideoKameras_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Tiešām vēlaties iziet?", "Iziet", MessageBoxButtons.YesNo);
            if (dialog == DialogResult.Yes)
            {
                Application.ExitThread();
            }
            else if (dialog == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        int i = 0;
        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            int height = 0;
            int width = 0;
            Pen p = new Pen(Brushes.Black, 0.5f);
            #region AdresesCol
            e.Graphics.FillRectangle(Brushes.LightGreen, new Rectangle (100,100,videoKamerasDataGridView.Columns[0].Width,videoKamerasDataGridView.Rows[0].Height));
            e.Graphics.DrawRectangle(p, new Rectangle(100, 100, videoKamerasDataGridView.Columns[0].Width, videoKamerasDataGridView.Rows[0].Height));
            e.Graphics.DrawString(videoKamerasDataGridView.Columns[0].HeaderText.ToString(),videoKamerasDataGridView.Font,Brushes.Black, new Rectangle(100, 100, videoKamerasDataGridView.Columns[0].Width, videoKamerasDataGridView.Rows[0].Height));
            #endregion
            #region AntenasIPCol
            e.Graphics.FillRectangle(Brushes.LightGreen, new Rectangle(100+videoKamerasDataGridView.Columns[0].Width, 100, videoKamerasDataGridView.Columns[1].Width, videoKamerasDataGridView.Rows[1].Height));
            e.Graphics.DrawRectangle(p, new Rectangle(100 + videoKamerasDataGridView.Columns[0].Width, 100, videoKamerasDataGridView.Columns[1].Width, videoKamerasDataGridView.Rows[1].Height));
            e.Graphics.DrawString(videoKamerasDataGridView.Columns[1].HeaderText.ToString(), videoKamerasDataGridView.Font, Brushes.Black, new Rectangle(100 + videoKamerasDataGridView.Columns[0].Width, 100, videoKamerasDataGridView.Columns[1].Width, videoKamerasDataGridView.Rows[1].Height));
            #endregion
            #region KamerasIPCol
            e.Graphics.FillRectangle(Brushes.LightGreen, new Rectangle(200 + videoKamerasDataGridView.Columns[0].Width, 100, videoKamerasDataGridView.Columns[2].Width, videoKamerasDataGridView.Rows[2].Height));
            e.Graphics.DrawRectangle(p, new Rectangle(200 + videoKamerasDataGridView.Columns[0].Width, 100, videoKamerasDataGridView.Columns[2].Width, videoKamerasDataGridView.Rows[2].Height));
            e.Graphics.DrawString(videoKamerasDataGridView.Columns[2].HeaderText.ToString(), videoKamerasDataGridView.Font, Brushes.Black, new Rectangle(200 + videoKamerasDataGridView.Columns[0].Width, 100, videoKamerasDataGridView.Columns[2].Width, videoKamerasDataGridView.Rows[2].Height));
            #endregion
            #region LietotajsCol
            e.Graphics.FillRectangle(Brushes.LightGreen, new Rectangle(300 + videoKamerasDataGridView.Columns[0].Width, 100, videoKamerasDataGridView.Columns[3].Width, videoKamerasDataGridView.Rows[3].Height));
            e.Graphics.DrawRectangle(p, new Rectangle(300 + videoKamerasDataGridView.Columns[0].Width, 100, videoKamerasDataGridView.Columns[3].Width, videoKamerasDataGridView.Rows[3].Height));
            e.Graphics.DrawString(videoKamerasDataGridView.Columns[3].HeaderText.ToString(), videoKamerasDataGridView.Font, Brushes.Black, new Rectangle(300 + videoKamerasDataGridView.Columns[0].Width, 100, videoKamerasDataGridView.Columns[3].Width, videoKamerasDataGridView.Rows[3].Height));
            #endregion
            #region ParoleCol
            e.Graphics.FillRectangle(Brushes.LightGreen, new Rectangle(400 + videoKamerasDataGridView.Columns[0].Width, 100, videoKamerasDataGridView.Columns[4].Width, videoKamerasDataGridView.Rows[4].Height));
            e.Graphics.DrawRectangle(p, new Rectangle(400 + videoKamerasDataGridView.Columns[0].Width, 100, videoKamerasDataGridView.Columns[4].Width, videoKamerasDataGridView.Rows[4].Height));
            e.Graphics.DrawString(videoKamerasDataGridView.Columns[4].HeaderText.ToString(), videoKamerasDataGridView.Font, Brushes.Black, new Rectangle(400 + videoKamerasDataGridView.Columns[0].Width, 100, videoKamerasDataGridView.Columns[4].Width, videoKamerasDataGridView.Rows[4].Height));
            #endregion
            #region KamerasNosCol
            e.Graphics.FillRectangle(Brushes.LightGreen, new Rectangle(500 + videoKamerasDataGridView.Columns[0].Width, 100, videoKamerasDataGridView.Columns[5].Width, videoKamerasDataGridView.Rows[5].Height));
            e.Graphics.DrawRectangle(p, new Rectangle(500 + videoKamerasDataGridView.Columns[0].Width, 100, videoKamerasDataGridView.Columns[5].Width, videoKamerasDataGridView.Rows[5].Height));
            e.Graphics.DrawString(videoKamerasDataGridView.Columns[5].HeaderText.ToString(), videoKamerasDataGridView.Font, Brushes.Black, new Rectangle(500 + videoKamerasDataGridView.Columns[0].Width, 100, videoKamerasDataGridView.Columns[5].Width, videoKamerasDataGridView.Rows[5].Height));
            #endregion
            height = 100;
            while (i<videoKamerasDataGridView.Rows.Count)
            {
                if (height>e.MarginBounds.Height)
                {
                    height = 100;
                    e.HasMorePages = true;
                    return;
                }
                height += videoKamerasDataGridView.Rows[0].Height;

                e.Graphics.DrawRectangle(p, new Rectangle(100, height, videoKamerasDataGridView.Columns[0].Width, videoKamerasDataGridView.Rows[0].Height));
                e.Graphics.DrawString(videoKamerasDataGridView.Rows[i].Cells[0].FormattedValue.ToString(), videoKamerasDataGridView.Font, Brushes.Black, new Rectangle(100, height, videoKamerasDataGridView.Columns[0].Width, videoKamerasDataGridView.Rows[0].Height));

                e.Graphics.DrawRectangle(p, new Rectangle(100 + videoKamerasDataGridView.Columns[0].Width, height, videoKamerasDataGridView.Columns[0].Width, videoKamerasDataGridView.Rows[0].Height));
                e.Graphics.DrawString(videoKamerasDataGridView.Rows[i].Cells[1].FormattedValue.ToString(), videoKamerasDataGridView.Font, Brushes.Black, new Rectangle(100 + videoKamerasDataGridView.Columns[0].Width, height, videoKamerasDataGridView.Columns[0].Width, videoKamerasDataGridView.Rows[0].Height));

                e.Graphics.DrawRectangle(p, new Rectangle(200 + videoKamerasDataGridView.Columns[0].Width, height, videoKamerasDataGridView.Columns[0].Width, videoKamerasDataGridView.Rows[0].Height));
                e.Graphics.DrawString(videoKamerasDataGridView.Rows[i].Cells[2].FormattedValue.ToString(), videoKamerasDataGridView.Font, Brushes.Black, new Rectangle(200 + videoKamerasDataGridView.Columns[0].Width, height, videoKamerasDataGridView.Columns[0].Width, videoKamerasDataGridView.Rows[0].Height));

                e.Graphics.DrawRectangle(p, new Rectangle(300 + videoKamerasDataGridView.Columns[0].Width, height, videoKamerasDataGridView.Columns[0].Width, videoKamerasDataGridView.Rows[0].Height));
                e.Graphics.DrawString(videoKamerasDataGridView.Rows[i].Cells[3].FormattedValue.ToString(), videoKamerasDataGridView.Font, Brushes.Black, new Rectangle(300 + videoKamerasDataGridView.Columns[0].Width, height, videoKamerasDataGridView.Columns[0].Width, videoKamerasDataGridView.Rows[0].Height));

                e.Graphics.DrawRectangle(p, new Rectangle(400 + videoKamerasDataGridView.Columns[0].Width, height, videoKamerasDataGridView.Columns[0].Width, videoKamerasDataGridView.Rows[0].Height));
                e.Graphics.DrawString(videoKamerasDataGridView.Rows[i].Cells[4].FormattedValue.ToString(), videoKamerasDataGridView.Font, Brushes.Black, new Rectangle(400 + videoKamerasDataGridView.Columns[0].Width, height, videoKamerasDataGridView.Columns[0].Width, videoKamerasDataGridView.Rows[0].Height));

                e.Graphics.DrawRectangle(p, new Rectangle(500 + videoKamerasDataGridView.Columns[0].Width, height, videoKamerasDataGridView.Columns[0].Width, videoKamerasDataGridView.Rows[0].Height));
                e.Graphics.DrawString(videoKamerasDataGridView.Rows[i].Cells[5].FormattedValue.ToString(), videoKamerasDataGridView.Font, Brushes.Black, new Rectangle(500 + videoKamerasDataGridView.Columns[0].Width, height, videoKamerasDataGridView.Columns[0].Width, videoKamerasDataGridView.Rows[0].Height));

                i++;


            }

        }

        private void mekletAdresiToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.videoKamerasTableAdapter.MekletAdresi(this.vidKamDataSet.VideoKameras, adresesToolStripTextBox.Text);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            DGVPrinter printer = new DGVPrinter();

            printer.Title = "Video kameras atskaite";

            printer.SubTitle = "te var pievienot vēl kādu tekstu!";

            printer.SubTitleFormatFlags = StringFormatFlags.LineLimit |

                                          StringFormatFlags.NoClip;

            printer.PageNumbers = true;

            printer.PageNumberInHeader = false;

            printer.PorportionalColumns = true;

            printer.HeaderCellAlignment = StringAlignment.Near;

            printer.Footer = "Kājenes informācija šeit, ja nepieciešama";

            printer.FooterSpacing = 15;



            printer.PrintDataGridView(videoKamerasDataGridView);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            PrintDialog printDialog = new PrintDialog();
            printDialog.Document = printDocument1;
            printDialog.UseEXDialog = true;
            //Get the document
            if (DialogResult.OK == printDialog.ShowDialog())
            {
                printDocument1.DocumentName = "Test Page Print";
                printDocument1.Print();
            }
        }
    }
}
