﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace VideoKameruRegistrs
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

#if DEBUG == false
            String dbPathMyDocs = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            //String dbPathAppData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            String dbPath = Path.Combine(dbPathMyDocs, "VidKamData");

            AppDomain.CurrentDomain.SetData("DataDirectory", dbPath);
#endif
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new VideoKameras());
        }
    }
}
