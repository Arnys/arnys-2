﻿namespace VideoKameruRegistrs
{
    partial class VideoKameras
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label adresesLabel;
            System.Windows.Forms.Label antenas_IPLabel;
            System.Windows.Forms.Label kameras_IPLabel;
            System.Windows.Forms.Label lietotajsLabel;
            System.Windows.Forms.Label paroleLabel;
            System.Windows.Forms.Label kameras_nosaukumsLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VideoKameras));
            this.videoKamerasBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.videoKamerasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vidKamDataSet = new VideoKameruRegistrs.VidKamDataSet();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.videoKamerasBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.videoKamerasDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.adresesTextBox = new System.Windows.Forms.TextBox();
            this.antenas_IPTextBox = new System.Windows.Forms.TextBox();
            this.kameras_IPTextBox = new System.Windows.Forms.TextBox();
            this.lietotajsTextBox = new System.Windows.Forms.TextBox();
            this.paroleTextBox = new System.Windows.Forms.TextBox();
            this.kameras_nosaukumsTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.mekletAdresiToolStrip = new System.Windows.Forms.ToolStrip();
            this.adresesToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            this.adresesToolStripTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.mekletAdresiToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.videoKamerasTableAdapter = new VideoKameruRegistrs.VidKamDataSetTableAdapters.VideoKamerasTableAdapter();
            this.tableAdapterManager = new VideoKameruRegistrs.VidKamDataSetTableAdapters.TableAdapterManager();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            adresesLabel = new System.Windows.Forms.Label();
            antenas_IPLabel = new System.Windows.Forms.Label();
            kameras_IPLabel = new System.Windows.Forms.Label();
            lietotajsLabel = new System.Windows.Forms.Label();
            paroleLabel = new System.Windows.Forms.Label();
            kameras_nosaukumsLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.videoKamerasBindingNavigator)).BeginInit();
            this.videoKamerasBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.videoKamerasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vidKamDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.videoKamerasDataGridView)).BeginInit();
            this.mekletAdresiToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // adresesLabel
            // 
            adresesLabel.AutoSize = true;
            adresesLabel.Location = new System.Drawing.Point(9, 62);
            adresesLabel.Name = "adresesLabel";
            adresesLabel.Size = new System.Drawing.Size(48, 13);
            adresesLabel.TabIndex = 4;
            adresesLabel.Text = "Adreses:";
            // 
            // antenas_IPLabel
            // 
            antenas_IPLabel.AutoSize = true;
            antenas_IPLabel.Location = new System.Drawing.Point(9, 88);
            antenas_IPLabel.Name = "antenas_IPLabel";
            antenas_IPLabel.Size = new System.Drawing.Size(62, 13);
            antenas_IPLabel.TabIndex = 6;
            antenas_IPLabel.Text = "Antenas IP:";
            // 
            // kameras_IPLabel
            // 
            kameras_IPLabel.AutoSize = true;
            kameras_IPLabel.Location = new System.Drawing.Point(9, 114);
            kameras_IPLabel.Name = "kameras_IPLabel";
            kameras_IPLabel.Size = new System.Drawing.Size(64, 13);
            kameras_IPLabel.TabIndex = 8;
            kameras_IPLabel.Text = "Kameras IP:";
            // 
            // lietotajsLabel
            // 
            lietotajsLabel.AutoSize = true;
            lietotajsLabel.Location = new System.Drawing.Point(9, 140);
            lietotajsLabel.Name = "lietotajsLabel";
            lietotajsLabel.Size = new System.Drawing.Size(49, 13);
            lietotajsLabel.TabIndex = 10;
            lietotajsLabel.Text = "Lietotajs:";
            // 
            // paroleLabel
            // 
            paroleLabel.AutoSize = true;
            paroleLabel.Location = new System.Drawing.Point(9, 166);
            paroleLabel.Name = "paroleLabel";
            paroleLabel.Size = new System.Drawing.Size(40, 13);
            paroleLabel.TabIndex = 12;
            paroleLabel.Text = "Parole:";
            // 
            // kameras_nosaukumsLabel
            // 
            kameras_nosaukumsLabel.AutoSize = true;
            kameras_nosaukumsLabel.Location = new System.Drawing.Point(9, 192);
            kameras_nosaukumsLabel.Name = "kameras_nosaukumsLabel";
            kameras_nosaukumsLabel.Size = new System.Drawing.Size(108, 13);
            kameras_nosaukumsLabel.TabIndex = 14;
            kameras_nosaukumsLabel.Text = "Kameras nosaukums:";
            // 
            // videoKamerasBindingNavigator
            // 
            this.videoKamerasBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.videoKamerasBindingNavigator.BindingSource = this.videoKamerasBindingSource;
            this.videoKamerasBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.videoKamerasBindingNavigator.CountItemFormat = "no {0}";
            this.videoKamerasBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.videoKamerasBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.videoKamerasBindingNavigatorSaveItem});
            this.videoKamerasBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.videoKamerasBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.videoKamerasBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.videoKamerasBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.videoKamerasBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.videoKamerasBindingNavigator.Name = "videoKamerasBindingNavigator";
            this.videoKamerasBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.videoKamerasBindingNavigator.Size = new System.Drawing.Size(934, 25);
            this.videoKamerasBindingNavigator.TabIndex = 0;
            this.videoKamerasBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Izveidot jaunu";
            // 
            // videoKamerasBindingSource
            // 
            this.videoKamerasBindingSource.DataMember = "VideoKameras";
            this.videoKamerasBindingSource.DataSource = this.vidKamDataSet;
            // 
            // vidKamDataSet
            // 
            this.vidKamDataSet.DataSetName = "VidKamDataSet";
            this.vidKamDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(38, 22);
            this.bindingNavigatorCountItem.Text = "no {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Dzēst";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Uz pirmo ierakstu";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Iepriekšējais";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Nākamais";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Uz pēdējo ierakstu";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // videoKamerasBindingNavigatorSaveItem
            // 
            this.videoKamerasBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.videoKamerasBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("videoKamerasBindingNavigatorSaveItem.Image")));
            this.videoKamerasBindingNavigatorSaveItem.Name = "videoKamerasBindingNavigatorSaveItem";
            this.videoKamerasBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.videoKamerasBindingNavigatorSaveItem.Text = "Saglabāt";
            this.videoKamerasBindingNavigatorSaveItem.Click += new System.EventHandler(this.videoKamerasBindingNavigatorSaveItem_Click);
            // 
            // videoKamerasDataGridView
            // 
            this.videoKamerasDataGridView.AutoGenerateColumns = false;
            this.videoKamerasDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.videoKamerasDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7});
            this.videoKamerasDataGridView.DataSource = this.videoKamerasBindingSource;
            this.videoKamerasDataGridView.Location = new System.Drawing.Point(249, 62);
            this.videoKamerasDataGridView.Name = "videoKamerasDataGridView";
            this.videoKamerasDataGridView.Size = new System.Drawing.Size(673, 266);
            this.videoKamerasDataGridView.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Adreses";
            this.dataGridViewTextBoxColumn2.HeaderText = "Adreses";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Antenas_IP";
            this.dataGridViewTextBoxColumn3.HeaderText = "Antenas IP";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Kameras_IP";
            this.dataGridViewTextBoxColumn4.HeaderText = "Kameras IP";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Lietotajs";
            this.dataGridViewTextBoxColumn5.HeaderText = "Lietotājs";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Parole";
            this.dataGridViewTextBoxColumn6.HeaderText = "Parole";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Kameras_nosaukums";
            this.dataGridViewTextBoxColumn7.HeaderText = "Kameras nosaukums";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 130;
            // 
            // adresesTextBox
            // 
            this.adresesTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.videoKamerasBindingSource, "Adreses", true));
            this.adresesTextBox.Location = new System.Drawing.Point(123, 59);
            this.adresesTextBox.Name = "adresesTextBox";
            this.adresesTextBox.Size = new System.Drawing.Size(100, 20);
            this.adresesTextBox.TabIndex = 5;
            // 
            // antenas_IPTextBox
            // 
            this.antenas_IPTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.videoKamerasBindingSource, "Antenas_IP", true));
            this.antenas_IPTextBox.Location = new System.Drawing.Point(123, 85);
            this.antenas_IPTextBox.Name = "antenas_IPTextBox";
            this.antenas_IPTextBox.Size = new System.Drawing.Size(100, 20);
            this.antenas_IPTextBox.TabIndex = 7;
            // 
            // kameras_IPTextBox
            // 
            this.kameras_IPTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.videoKamerasBindingSource, "Kameras_IP", true));
            this.kameras_IPTextBox.Location = new System.Drawing.Point(123, 111);
            this.kameras_IPTextBox.Name = "kameras_IPTextBox";
            this.kameras_IPTextBox.Size = new System.Drawing.Size(100, 20);
            this.kameras_IPTextBox.TabIndex = 9;
            // 
            // lietotajsTextBox
            // 
            this.lietotajsTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.videoKamerasBindingSource, "Lietotajs", true));
            this.lietotajsTextBox.Location = new System.Drawing.Point(123, 137);
            this.lietotajsTextBox.Name = "lietotajsTextBox";
            this.lietotajsTextBox.Size = new System.Drawing.Size(100, 20);
            this.lietotajsTextBox.TabIndex = 11;
            // 
            // paroleTextBox
            // 
            this.paroleTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.videoKamerasBindingSource, "Parole", true));
            this.paroleTextBox.Location = new System.Drawing.Point(123, 163);
            this.paroleTextBox.Name = "paroleTextBox";
            this.paroleTextBox.Size = new System.Drawing.Size(100, 20);
            this.paroleTextBox.TabIndex = 13;
            // 
            // kameras_nosaukumsTextBox
            // 
            this.kameras_nosaukumsTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.videoKamerasBindingSource, "Kameras_nosaukums", true));
            this.kameras_nosaukumsTextBox.Location = new System.Drawing.Point(123, 189);
            this.kameras_nosaukumsTextBox.Name = "kameras_nosaukumsTextBox";
            this.kameras_nosaukumsTextBox.Size = new System.Drawing.Size(100, 20);
            this.kameras_nosaukumsTextBox.TabIndex = 15;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 307);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(220, 23);
            this.button1.TabIndex = 16;
            this.button1.Text = "Kameru atrašanās vietas";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(13, 220);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(220, 23);
            this.button2.TabIndex = 17;
            this.button2.Text = "Printēt (nestradā) (visual studio piedāvātais)";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Document = this.printDocument1;
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // mekletAdresiToolStrip
            // 
            this.mekletAdresiToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.mekletAdresiToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.adresesToolStripLabel,
            this.adresesToolStripTextBox,
            this.mekletAdresiToolStripButton});
            this.mekletAdresiToolStrip.Location = new System.Drawing.Point(0, 25);
            this.mekletAdresiToolStrip.Name = "mekletAdresiToolStrip";
            this.mekletAdresiToolStrip.Size = new System.Drawing.Size(369, 25);
            this.mekletAdresiToolStrip.TabIndex = 18;
            this.mekletAdresiToolStrip.Text = "mekletAdresiToolStrip";
            // 
            // adresesToolStripLabel
            // 
            this.adresesToolStripLabel.Name = "adresesToolStripLabel";
            this.adresesToolStripLabel.Size = new System.Drawing.Size(51, 22);
            this.adresesToolStripLabel.Text = "Adreses:";
            // 
            // adresesToolStripTextBox
            // 
            this.adresesToolStripTextBox.Name = "adresesToolStripTextBox";
            this.adresesToolStripTextBox.Size = new System.Drawing.Size(100, 25);
            // 
            // mekletAdresiToolStripButton
            // 
            this.mekletAdresiToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.mekletAdresiToolStripButton.Name = "mekletAdresiToolStripButton";
            this.mekletAdresiToolStripButton.Size = new System.Drawing.Size(204, 22);
            this.mekletAdresiToolStripButton.Text = "Meklēt pēc adreses / atrašanās vietas";
            this.mekletAdresiToolStripButton.Click += new System.EventHandler(this.mekletAdresiToolStripButton_Click);
            // 
            // videoKamerasTableAdapter
            // 
            this.videoKamerasTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.UpdateOrder = VideoKameruRegistrs.VidKamDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VideoKamerasTableAdapter = this.videoKamerasTableAdapter;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(12, 249);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(221, 23);
            this.button3.TabIndex = 19;
            this.button3.Text = "Printēt (strādā, nav previev, dgv.dll)";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(13, 278);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(220, 23);
            this.button4.TabIndex = 20;
            this.button4.Text = "printēt (nestrādā)";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // VideoKameras
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(934, 442);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.mekletAdresiToolStrip);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(adresesLabel);
            this.Controls.Add(this.adresesTextBox);
            this.Controls.Add(antenas_IPLabel);
            this.Controls.Add(this.antenas_IPTextBox);
            this.Controls.Add(kameras_IPLabel);
            this.Controls.Add(this.kameras_IPTextBox);
            this.Controls.Add(lietotajsLabel);
            this.Controls.Add(this.lietotajsTextBox);
            this.Controls.Add(paroleLabel);
            this.Controls.Add(this.paroleTextBox);
            this.Controls.Add(kameras_nosaukumsLabel);
            this.Controls.Add(this.kameras_nosaukumsTextBox);
            this.Controls.Add(this.videoKamerasDataGridView);
            this.Controls.Add(this.videoKamerasBindingNavigator);
            this.ForeColor = System.Drawing.Color.Black;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "VideoKameras";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Video kameras";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VideoKameras_FormClosing);
            this.Load += new System.EventHandler(this.VideoKameras_Load);
            ((System.ComponentModel.ISupportInitialize)(this.videoKamerasBindingNavigator)).EndInit();
            this.videoKamerasBindingNavigator.ResumeLayout(false);
            this.videoKamerasBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.videoKamerasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vidKamDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.videoKamerasDataGridView)).EndInit();
            this.mekletAdresiToolStrip.ResumeLayout(false);
            this.mekletAdresiToolStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private VidKamDataSet vidKamDataSet;
        private System.Windows.Forms.BindingSource videoKamerasBindingSource;
        private VidKamDataSetTableAdapters.VideoKamerasTableAdapter videoKamerasTableAdapter;
        private VidKamDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator videoKamerasBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton videoKamerasBindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView videoKamerasDataGridView;
        private System.Windows.Forms.TextBox adresesTextBox;
        private System.Windows.Forms.TextBox antenas_IPTextBox;
        private System.Windows.Forms.TextBox kameras_IPTextBox;
        private System.Windows.Forms.TextBox lietotajsTextBox;
        private System.Windows.Forms.TextBox paroleTextBox;
        private System.Windows.Forms.TextBox kameras_nosaukumsTextBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.ToolStrip mekletAdresiToolStrip;
        private System.Windows.Forms.ToolStripLabel adresesToolStripLabel;
        private System.Windows.Forms.ToolStripTextBox adresesToolStripTextBox;
        private System.Windows.Forms.ToolStripButton mekletAdresiToolStripButton;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
    }
}

